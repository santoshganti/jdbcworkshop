##Installing sql server jdbc driver into local repository of maven
* Download the sql server driver from Microsoft website 
* Goto the path where the sqljdbc4.jar is present and enter the following command

 `mvn install:install-file -Dfile=sqljdbc4.jar -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc4 -Dversion=4.2 -Dpackaging=jar`

* Now navigate to the pom.xml file and add the following lines

`
                <dependency>
			<groupId>com.microsoft.sqlserver</groupId>
			<artifactId>sqljdbc4</artifactId>
			<version>4.2</version>
		</dependency>
`
