package org.au.workshop.jdbc;

import java.sql.*;
import java.util.ArrayList;

import org.au.workshop.domain.Employee;

//Prepared Statements into table
public class Kata06PreparedStatementsDemo extends Kata05ClosingResourcesDemo {

	protected void insertUsingPreparedStatement(int id, int age, String firstName, String lastName) {
		// TODO - implement this method to insert using parameterized queries
	}

	protected void updateUsingPreparedStatements(String firstName, String lastName, int age, int id) {
		// TODO - similar to the above method just update instead of insert
	}

	protected void deleteUsingPreapredStatement(int id) {
		// TODO - delete a row based on id
	}

	protected ArrayList<Employee> retrieveUsingPreparedStatements() {
		return null;
		// TODO - retrieve, store the data in employee object and then add the
		// object in array
		// Repeat until there are rows
	}

}
