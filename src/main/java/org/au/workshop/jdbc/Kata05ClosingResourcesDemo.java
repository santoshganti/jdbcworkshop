package org.au.workshop.jdbc;

import java.sql.SQLException;

/**
 * In this kata you will learn about closing resources
 */
public class Kata05ClosingResourcesDemo extends Kata02StatementDemo {

	public boolean closeConection() throws SQLException {
		return false;
		// TODO - Implement this method which returns boolean value , true if
		// connection close else false
	}

	public boolean closeStatement() throws SQLException {
		return false;
		// TODO - Implement this method which returns boolean value , true if
		// statement close else false
	}

	public boolean closeResultSet() throws SQLException {
		return false;
		// TODO - Implement this method which returns boolean value , true if
		// resultset close else false
	}

	public void closeResources() {
		// TODO - Since we have learned about closing resources, we will
		// implement this method and call this everywhere when we need to close
		// resources
	}
}
